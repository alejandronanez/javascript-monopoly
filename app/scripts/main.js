// Initial code for the Monopoly Game Project
var MONOPOLY = MONOPOLY || {};

/**
 * Get all the events in the DOM here.
 */
MONOPOLY.Starter = function () {

    var placesNumber  = document.getElementById('places-number'),
        playersNumber = document.getElementById('players-number'),
        configForm    = document.getElementById('config-app'),
        playersArray  = [],
        placesArray   = [],

        /**
         * Create all the players
         * @param  {Integer} playersNumber Number of players to create
         */
        createPlayers = function (playersNumber) {
            var i = 0,
                player = {},
                options = {};
            // Create the players
            for (i; i < playersNumber; i = i + 1) {
                options.name = 'Player #' + i;
                options.type = 'default';
                player = new MONOPOLY.Player(options);
                playersArray.push(player);
            }
        },
        /**
         * Create all the places
         * @param  {Integer} placesNumber Number of places to create
         */
        createPlaces = function (placesNumber) {
            var i = 0,
                places = {};

            // Mayor's Office
            place = new MONOPOLY.Place.PlaceFactory('MayorOffice', {});
            placesArray.push(place);

            // Default Places
            for (i; i < placesNumber; i = i + 1) {
                place = new MONOPOLY.Place.PlaceFactory('Default', {name: 'Place - ' + (i+1)});
                placesArray.push(place);
            }

            // Police Station
            place = new MONOPOLY.Place.PlaceFactory('PoliceStation', {});
            placesArray.push(place);

            // Jail
            place = new MONOPOLY.Place.PlaceFactory('Jail', {});
            placesArray.push(place);
        },
        /**
         * Initialize the application
         */
        initialize = function () {
            var board = new MONOPOLY.Board();

            configForm.addEventListener('submit', function (e) {
                e.preventDefault();
                if (playersNumber.value > 0 && playersNumber.value < 5 && placesNumber.value > 2) {
                    // Create players
                    createPlayers( parseInt(playersNumber.value) );
                    // Create places
                    createPlaces( parseInt(placesNumber.value) );
                    // Create places
                    board.draw(placesArray);

                    // Show the DICE MODULE
                    document.getElementById('roll-dice').classList.remove('hide');

                    // Initialize the MONOPOLY.GameInteraction module
                    MONOPOLY.GameInteraction.init();
                } else {
                    alert('Please remember:\n# of players: 2 - 4\n# of places > 2 ');
                }
            });

        };

        return {
            initialize: initialize,
            players: playersArray,
            places: placesArray
        }
}();

// Initialize the Application
MONOPOLY.Starter.initialize();