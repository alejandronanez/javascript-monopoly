var MONOPOLY = MONOPOLY || {};

/**
 * Monopoly Player Constructor
 * @param  {Object} options
 */
MONOPOLY.Player = function (options) {
    this.active = true;
    this.cash  = options.cash || 10000;
    this.color = options.color || 'grey';
    this.name  = options.name || 'Player Default Name';
    this.type  = options.type || 'bank'; 
};

/**
 * Monopoly Player Prototype
 * @return {Object}
 */
MONOPOLY.Player.prototype = function () {

    var /**
         * Subtract 'quantity' value from the user cash
         * @param {Integer} quantity
         */
        subtractCash = function (quantity) {
            this.cash = this.cash - quantity;
        },
        /**
         * Does the player still have cash?
         * @return {Boolean}
         */
        _hasCash = function () {
            if (this.cash < 0) {
                this.active = false;
            }
        },
        /**
         * Is the user still active
         * @return {Boolean}
         */
        isActive = function () {
            _hasCash.call(this);
            return this.isActive;
        };

    return {
        isActive: isActive,
        subtract: subtractCash
    };
}();