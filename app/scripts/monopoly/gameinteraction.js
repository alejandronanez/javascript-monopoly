var MONOPOLY = MONOPOLY || {};

/**
 * Monopoly Game Interaction constructor
 */
MONOPOLY.GameInteraction = function () {

    // How many players
    this.totalPlayers = MONOPOLY.Starter.players.length;
    // How many places
    this.totalPlaces  = MONOPOLY.Starter.places.length;
    // Places
    this.places       = MONOPOLY.Starter.places;
    // Players
    this.players      = MONOPOLY.Starter.players;

    // Dices
    this.diceOne      = new MONOPOLY.Dice();
    this.diceTwo      = new MONOPOLY.Dice();
};

/**
 * Monopoly Game Interaction prototype
 * @return {Object}
 */
MONOPOLY.GameInteraction.prototype = function () {
    // Draw table for the users
    
    // Roll Dices for each user

    // Calcaulate the new $ for the user

    // Check if the user is in the Jail

    // Check if the user get a pair after rolling dices

    // Check if the property is from the bank
    // 
}();