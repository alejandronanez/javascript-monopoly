var MONOPOLY = MONOPOLY || {};

/**
 * Monopoly Board constructor
 */
MONOPOLY.Board = function () {};

/**
 * Monopoly Board Prototype
 */
MONOPOLY.Board.prototype = function () {

        /**
         * Fill the board with the places data
         * @param  {Object} place The place to draw
         */ 
        _fillTemplate = function (place) {

            if (place.placeType === 'default') {
                var placeOwner = place.getPlaceOwner(),
                    placePrice = place.singlePrice,
                    placeName = place.name,
                    template = document.getElementById('board-template').childNodes[1].cloneNode(true),
                    target = document.getElementById('board');

                // Fill the template
                template.getElementsByTagName('h3')[0].innerHTML = placeName;
                template.getElementsByClassName('property-owner')[0].getElementsByTagName('span')[0].innerHTML = placeOwner;
                template.getElementsByClassName('property-price')[0].getElementsByTagName('span')[0].innerHTML = placePrice;
                template.getElementsByClassName('property-rent')[0].getElementsByTagName('span')[0].innerHTML = 'Property Rent';

                // Attach the new element into the HTML
                target.appendChild(template);
            }
            
        },
        /**
         * Iterate through all the places and draw them
         * @param  {Array} placesArray All the places
         */
        draw = function (placesArray) {
            var i = 0;
            for( i in placesArray ) {
                _fillTemplate.call(this, placesArray[i]);
            }
            // Disable the configuration submit button.
            document.getElementById('configuration-submit').setAttribute('disabled');
        };

    return {
        draw: draw
    };
}();